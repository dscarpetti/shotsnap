(ns shotsnap.webcam
  (:import
   [com.github.sarxos.webcam Webcam WebcamPanel WebcamResolution WebcamMotionDetector WebcamMotionEvent WebcamMotionListener]
   [java.awt Color]
   [java.awt.image BufferedImage]
   [javax.swing JFrame]))

(defonce webcam (atom nil))

(defn init! []
  (when @webcam
    (.close ^Webcam @webcam))
  (let [default-webcam (Webcam/getDefault)]
    (.setViewSize default-webcam (.getSize WebcamResolution/QVGA))
    (.open default-webcam)
    (reset! webcam default-webcam)))

(defn show! []
  (let [webcam @webcam
        webcam-panel (WebcamPanel. webcam)
        jframe (JFrame. "Webcam")]

    (.setFPSDisplayed webcam-panel true)
    (.setDisplayDebugInfo webcam-panel true)
    (.setImageSizeDisplayed webcam-panel true)
    (.setMirrored webcam-panel true)

    (.add jframe webcam-panel)
    (.setResizable jframe true)
    (.pack jframe)
    (.setVisible jframe true)))

(defn get-reddest-pixels! []
  (let [img ^java.awt.image.BufferedImage (.getImage ^Webcam @webcam)
        width (.getWidth img)
        height (.getHeight img)
        [intensity coords]
        (loop [w 0
               h 0
               max 0
               coords []]
          (cond
            (= h height) [max coords]
         (= w width) (recur 0 (inc h) max coords)
         :else
         (let [v (.getRed (Color. (.getRGB img w h)))]
           (cond
             (> v max) (recur (inc w) h v [[w h]])
             (= v max) (recur (inc w) h v (conj coords [w h]))
             :else (recur (inc w) h max coords)))))]

    {:img img
     :coords coords
     :intensity intensity}))

(defn get-image! []
  (.getImage ^Webcam @webcam))


(defn get-red-spike [prev]
  (let [prev (or prev (repeat 0))
        img ^java.awt.image.BufferedImage (.getImage ^Webcam @webcam)
        width (.getWidth img)
        height (.getHeight img)
        [reds dmax x y]
        (loop [w 0
               h 0
               reds (transient [])
               prev prev
               dmax 0
               x 0
               y 0
               ]
          (cond
            (= h height) [(persistent! reds) dmax x y]
            (= w width) (recur 0 (inc h) reds prev dmax x y)
            :else
            (let [r (.getRed (Color. (.getRGB img w h)))
                  d (- r (first prev))]
              (if (> d dmax)
                (recur (inc w) h (conj! reds r) (rest prev) d w h)
                (recur (inc w) h (conj! reds r) (rest prev) dmax x y)))))]
    [img reds dmax x y]))
;;    {:img img :reds reds :dmax dmax :x x :y y}))

(def prev-reds (atom nil))

(defn get-red-spike! []
  (let [[img reds dmax x y] (get-red-spike @prev-reds)
        dmax (if (nil? @prev-reds) 0 dmax)]
    (reset! prev-reds reds)
    {:img img
     :dmax dmax
     :x x
     :y y}))


;;;;;

(defn corner-comp [xcmp ycmp coords]
  (let [{:keys [x y]}
        (reduce (fn [p [x y intensity]]
                  (if (and
                       (>= intensity (:i p))
                       (xcmp x (:x p))
                       (ycmp y (:y p)))
                    {:i intensity :x x :y y}
                    p))
                {:i 0
                 :x 160
                 :y 120}
                coords)]
    [x y]))


(defn get-red-corners! []
  (let [img ^java.awt.image.BufferedImage (.getImage ^Webcam @webcam)
        width (.getWidth img)
        height (.getHeight img)
        coords (persistent!
                (loop [w 0
                       h 0
                       coords (transient [])]
                  (cond
                    (= h height) coords
                    (= w width) (recur 0 (inc h) coords)
                    :else
                    (let [c (Color. (.getRGB img w h))
                          r (.getRed c)
                          g (.getGreen c)
                          b (.getBlue c)]
                      (recur (inc w)
                             h
                             (conj! coords [w h (- r g b )]))))))]
    {:lt (corner-comp < < coords)
     :rt (corner-comp > < coords)
     :lb (corner-comp < > coords)
     :rb (corner-comp > > coords)}))


(defn corner-comp-2 [xcmp ycmp coords]
  (let [{:keys [x y]}
        (reduce (fn [p [x y]]
                  (if (and
                       (xcmp x (:x p))
                       (ycmp y (:y p)))
                    {:x x :y y}
                    p))
                {:x 160
                 :y 120}
                coords)]
    [x y]))


(def box (atom nil))

(defn flood-fill! [img target-color fill-color w h x y]
  (loop [[p & pts] (list [x y])]
    (when-let [[x y] p]
      (if (and
           (not (or (>= x w)
                    (< x 0)
                    (>= y h)
                    (< y 0)))
           (= (.getRGB ^java.awt.image.BufferedImage img x y) target-color))
        (do
          (.setRGB  ^java.awt.image.BufferedImage img (first p) (second p) fill-color)
          (recur (conj pts [(inc x) y] [(dec x) y] [x (inc y)] [x (dec y)])))
        (recur pts)))))


(defn flood-fill-faster! [img target-color fill-color w h x y]
  (loop [[p & pts] (list [x y :none])]
    (when-let [[x y d] p]
      (if (and
           (not (or (>= x w)
                    (< x 0)
                    (>= y h)
                    (< y 0)))
           (= (.getRGB ^java.awt.image.BufferedImage img x y) target-color))
        (do
          (.setRGB  ^java.awt.image.BufferedImage img (first p) (second p) fill-color)
          (condp = d
            :right (recur (conj pts [(dec x) y :left] [x (inc y) :down] [x (dec y) :up]))
            :left (recur (conj pts [(dec x) y :left] [x (inc y) :down] [x (dec y) :up]))
            :down (recur (conj pts [(inc x) y :right] [(dec x) y :left] [x (inc y) :down]))
            :up (recur (conj pts [(inc x) y :right] [(dec x) y :left] [x (dec y) :up]))
            (recur (conj pts [(inc x) y :right] [(dec x) y :left] [x (inc y) :down] [x (dec y) :up]))))
        (recur pts)))))


(defn clone-buffer [img]
  (let [cm (.getColorModel img)
        iap (.isAlphaPremultiplied cm)
        raster (.copyData img nil)]
    (BufferedImage. cm raster iap nil)))


(defn pyth [x1 y1 x2 y2]
  (Math/sqrt (+ (Math/pow (- x2 x1) 2) (Math/pow (- y2 y1) 2))))

(defn get-corners [img fill-color]
  (let [width (.getWidth img)
        height (.getHeight img)
        hw (quot width 2)
        hh (quot height 2)
        start-pt [hw hh]
        start-d (pyth 0 0 hw hh) ]
    (loop [w 0
           h 0
           tl-p start-pt
           tl-d start-d
           tr-p start-pt
           tr-d start-d
           bl-p start-pt
           bl-d start-d
           br-p start-pt
           br-d start-d]

;;      (if (= w 100)
;;        (println (.getRGB img w h)))
      (cond
        (= h height) {:tl tl-p :tr tr-p :bl bl-p :br br-p}
        (= w width) (recur 0 (inc h) tl-p tl-d tr-p tr-d bl-p bl-d br-p br-d)


        (= (.getRGB img w h) fill-color)
        (let [tl (pyth 0 0 w h)
              tr (pyth width 0 w h)
              bl (pyth 0 height w h)
              br (pyth width height w h)]
          (cond
            (< tl tl-d) (recur (inc w) h
                               [w h] tl
                               tr-p tr-d
                               bl-p bl-d
                               br-p br-d)
            (< tr tr-d) (recur (inc w) h
                               tl-p tl-d
                               [w h] tr
                               bl-p bl-d
                               br-p br-d)
            (< bl bl-d) (recur (inc w) h
                               tl-p tl-d
                               tr-p tr-d
                               [w h] bl
                               br-p br-d)
            (< br br-d) (recur (inc w) h
                               tl-p tl-d
                               tr-p tr-d
                               bl-p bl-d
                               [w h] br)
            :else (recur (inc w) h
                         tl-p tl-d
                         tr-p tr-d
                         bl-p bl-d
                         br-p br-d)))
        :else (recur (inc w) h tl-p tl-d tr-p tr-d bl-p bl-d br-p br-d)))))






(defn get-box! []
  (let [img ^java.awt.image.BufferedImage (.getImage ^Webcam @webcam)
        orig (clone-buffer img)
        width (.getWidth img)
        height (.getHeight img)
        match-color (.getRGB (Color. 255 0 0))
        fill-color (.getRGB (Color. 0 255 255))
        empty-color (.getRGB (Color. 0 0 0))]
    (loop [w 0
           h 0]
      (cond
        (= h height) nil
        (= w width) (recur 0 (inc h))
        :else
        (let [c (Color. (.getRGB img w h))
              r (.getRed c)
              g (.getGreen c)
              b (.getBlue c)
              v (if (pos? (- r g b)) true false)]
          (.setRGB img w h (if v match-color empty-color))
          (recur (inc w) h))))
    (flood-fill-faster! img match-color fill-color width height (quot width 2) (quot height 2))
    (let [{:keys [tl tr bl br]} (get-corners img fill-color)]
      (reset! box {:width width
                   :height height
                   :img orig
                   :tl tl
                   :tr tr
                   :bl bl
                   :br br}))))

(defn get-red-box! []
  (get-box!))
