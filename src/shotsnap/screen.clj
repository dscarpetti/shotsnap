(ns shotsnap.screen
  (:require
   [quil.core :as q]
   [quil.middleware :as qm]
   [shotsnap.webcam :as webcam])
  (:import
   [java.awt Robot Image]
   [java.awt.event InputEvent]
   [processing.core PImage]))

(def shot-threshold (atom 0))

(defn calibrate-shot-threshold! [iterations]
  (loop [n iterations
         ds 0]
    (if (zero? n)
      (reset! shot-threshold (quot (* 1.5 ds) iterations))
      (let [{:keys [dmax]} (webcam/get-red-spike!)]
        (Thread/sleep 100)
        (recur (dec n) (+ ds dmax))))))


(defn calibrate-screen [state]
  (let [s 50
        w (q/width)
        h (q/height)
        {:keys [tl tr br bl img]} state
        ]
    (q/stroke-weight 1)

    (q/fill 255 0 0)
    (q/stroke 255 0 0)
    (q/rect 0 0 720 720)
    (q/rect (- w s) 0 s s)
    (q/rect 0 (- h s) s s)
    (q/rect (- w s) (- h s) s s)


    (q/stroke-weight 3)
    (q/push-matrix)
    (q/translate 100 900)
    (q/image (PImage. img) 0 0)
    (q/stroke 0 255 0)
    (q/line tl tr)
    (q/line tr br)
    (q/line br bl)
    (q/line bl tl)


    (q/stroke-weight 3)
    (q/stroke 255)
    (q/line [150 110] [170 130])
    (q/line [170 110] [150 130])


    (q/pop-matrix)


    state))



(defn setup-sketch []
  (q/frame-rate 10)
  (q/background 255)
;;  (calibrate-shot-threshold! 10)
  {:bullet-holes []
   :calibrating-screen true})

(defn update-sketch [state]
  (if (:calibrating-screen state)
    (assoc (webcam/get-red-box!)
           :calibrating-screen true)
    (let [{:keys [img dmax x y]} (webcam/get-red-spike!)
          state (assoc state :img (PImage. img))]
      (if (> dmax @shot-threshold)
        (assoc state :bullet-holes [[x y]])
        state))))

(defn draw-sketch [state]
  (q/background 0)
  (if (:calibrating-screen state)
    (calibrate-screen state)
    (do
      (try
        (q/image (:img state) 0 0)
        (catch Throwable t))
      (doseq [[x y] (:bullet-holes state)]
        (q/ellipse x y 10 10)))))

(defn handle-mouse-press [state {:keys [x y button] :as evt}]
  (update state :bullet-holes conj [x y]))


(defn run []
   (q/sketch
   :size [720 1440]
   :setup setup-sketch
   :update #(update-sketch %)
   :draw #(draw-sketch %)
   :mouse-pressed #(handle-mouse-press %1 %2)
   ;;:features [:present]
   :middleware [qm/fun-mode]))

(def robot (Robot.))

(defn click-mouse! [x y]
  (.mouseMove ^Robot robot x y)
  (.mousePress ^Robot robot InputEvent/BUTTON1_MASK)
  (.mouseRelease ^Robot robot InputEvent/BUTTON1_MASK))
